var WordClock = function() {
	this.$container = $('#wordclock .clock');

	this.updateWords = function() {
		var ctime = this.getTime();
		var actives = '';
		var activesArray = ['it', 'is'];
		var h = ctime.getHours();
		var m = ctime.getMinutes();

		// Minutes
		switch ( m ) {
			case 0:
				activesArray.push('oclock');
				break;
			case 5:
				activesArray.push('five', 'past');
				break;
			case 10:
				activesArray.push('ten', 'past');
				break;
			case 15:
				activesArray.push('quarter', 'past');
				break;
			case 20:
				activesArray.push('twenty', 'past');
				break;
			case 25:
				activesArray.push('twentyfive', 'past');
				break;
			case 30:
				activesArray.push('half', 'past');
				break;
			case 35:
				h++;
				activesArray.push('twentyfive', 'to');
				break;
			case 40:
				h++;
				activesArray.push('twenty', 'to');
				break;
			case 45:
				h++;
				activesArray.push('quarter', 'to');
				break;
			case 50:
				h++;
				activesArray.push('ten', 'to');
				break;
			case 55:
				h++;
				activesArray.push('five', 'to');
				break;
			case 60:
				h++;
				activesArray.push('oclock');
				break;
		}

		// Hour
		if ( h > 12 ) h -= 12;
		if ( h == 0 ) h = 12; // Midnight is 12 (o' clock), too
		activesArray.push('hour-' + h);

		this.$container.find('td.active').removeClass('active');
		// this.$container.find(actives).addClass('active');
		for ( i = 0; i < activesArray.length; i++ ) {
			console.log();
			$('.' + activesArray[i]).addClass('active');
		}
	}

	this.getTime = function(minutes) {
		minutes = typeof minutes !== 'undefined' ? minutes : 5;

		// Thanks to Tomasz Nurkiewicz @ http://stackoverflow.com/a/10789415/1358948
		var coeff = 1000 * 60 * minutes;
		return new Date(Math.round(new Date().getTime() / coeff) * coeff);
	}
}