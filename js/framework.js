$(function() {
	// WordClock
	var wordclock = new WordClock();
		wordclock.updateWords();

	setInterval(function() {
		wordclock.updateWords();
	}, 10000);

	// Themes
	themeManager = new ThemeManager();
	$('#wordclock .clock').on('click', function() {
		themeManager.nextTheme();
	});
});

var minimalTimeout;
$('html, body').on('mousemove', function() {
	$('body').removeClass('minimal');
    clearTimeout(minimalTimeout);
    minimalTimeout = setTimeout(function() {
    	$('body').addClass('minimal');
    }, 5000);
});

var ThemeManager = function() {
	this.themes = ['', 'inverted', 'onefinity'];
	this.currentTheme = 0;

	this.nextTheme = function() {
		this.currentTheme = (typeof this.themes[this.currentTheme+1] != 'undefined') ? this.currentTheme+1 : 0;
		this.setTheme();
	}

	this.setTheme = function() {
		$('body').attr('data-theme', this.themes[this.currentTheme]);
	}
}