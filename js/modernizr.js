Modernizr.load(
	{
		test: Modernizr.cssremunit,
		nope: './js/polyfill/rem.min.js'
	},
	{
		test: Modernizr.input.placeholder,
		nope: '.js/polyfill/placeholder.min.js'
	}
);